﻿using System;

namespace Practice_02
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1.
            Console.WriteLine("Дано уравнение y=7x^2-3x+4");
            Console.Write("Введите значение x: ");
            double variableValue = double.Parse(Console.ReadLine());
            double resultY = 7 * Math.Pow(variableValue, 2) - 3 * variableValue + 4;
            Console.WriteLine("Результат: " + resultY);
            Console.WriteLine("-------------");

            // 2.
            Console.Write("Введите значение радиуса окружности: ");
            double radiusCircle = double.Parse(Console.ReadLine());
            double circumference = 2 * Math.PI * radiusCircle;
            double areaOfCircle = Math.PI * Math.Pow(radiusCircle, 2);
            Console.WriteLine("Длина окружности: " + circumference);
            Console.WriteLine("Площадь окружности: " + areaOfCircle);
            Console.WriteLine("-------------");

            // 3.
            Console.Write("Введите значение в сантиметрах: ");
            int valueInCentimeters = int.Parse(Console.ReadLine());
            int valueInMeters = valueInCentimeters / 100;
            Console.WriteLine("Значение в метрах: " + valueInMeters);
            Console.WriteLine("-----------");

            // 4.
            Console.Write("За 234 дня прошло полных недель: ");
            int countOfWeeks = 234 / 7;
            Console.WriteLine(countOfWeeks);
            Console.WriteLine("-----------");

            // 5.
            Console.Write("Введите двухзначное число: ");
            int number = int.Parse(Console.ReadLine());
            if (number > 9 && number < 100)
            {
                int numberOfTeens = number / 10;
                int numberOfUnits = number % 10;
                int sumOfDigits = numberOfTeens + numberOfUnits;
                int productOfNumbers = numberOfTeens * numberOfUnits;
                Console.Write("a) число десятков в нем - " + numberOfTeens +
                    "\nb) число единиц в нем - " + numberOfUnits +
                    "\nc) сумму его цифр - " + sumOfDigits +
                    "\nd) произведение его цифр - " + productOfNumbers + "\n");
            }
            else
                Console.WriteLine("Было введено некорректное число");
            Console.WriteLine("-----------");

            // 6.
            Console.Write("Введите четырехзначное число: ");
            int evenNumber = int.Parse(Console.ReadLine());
            int sumEvenNumber = 0;
            int compositionEvenNumber = 1;
            if (evenNumber > 999 && evenNumber < 10000)
            {
                while (evenNumber >= 10)
                {
                    sumEvenNumber += evenNumber % 10;
                    compositionEvenNumber *= evenNumber % 10;
                    evenNumber /= 10;
                }
                sumEvenNumber += evenNumber;
                compositionEvenNumber *= evenNumber;
                Console.Write("a) сумма его цифр - " + sumEvenNumber +
                    "\nb) произведение его цифр - " + compositionEvenNumber + "\n");
            }
            else
                Console.WriteLine("Было введено некорректное число");
            Console.WriteLine("-----------");

            // 7.
            Console.WriteLine("В трехзначном числе x зачеркнули его вторую цифру." +
                              " Когда к образованному при этом двузначному числу справа приписали вторую цифру числа x, то получилось число 456.");
            int numberOne = 4;
            int numberTwo = 5;
            int numberThree = 6;
            int resultX = numberOne * 100 + numberThree * 10 + numberTwo;
            Console.WriteLine("Числом X является число - " + resultX);
            Console.WriteLine("-----------");

            // 8.

            Console.Write("Введите логическое значение X (true/false): ");
            bool valueX = bool.Parse(Console.ReadLine());
            Console.Write("Введите логическое значение Y (true/false): ");
            bool valueY = bool.Parse(Console.ReadLine());
            bool resultA = !valueX && !valueY;
            bool resultB = valueX || (!(valueX && valueY));
            bool resultC = (!(valueX && valueY)) || valueY;
            Console.WriteLine("a) "+resultA);
            Console.WriteLine("b) "+resultB);
            Console.WriteLine("c) "+resultC);


        }
    }
}
